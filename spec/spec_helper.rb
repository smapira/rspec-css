# frozen_string_literal: true

$LOAD_PATH.unshift File.expand_path('../lib', __dir__)

require 'rspec/css'
require 'capybara/rspec'
require 'webdrivers/chromedriver'

class MatchersMock < BasicObject
  attr_reader :defined_matchers,
              :match_blocks,
              :failure_message_blocks,
              :failure_message_when_negated_blocks,
              :description_blocks,
              :diffable_blocks,
              :value,
              :context

  def define(name, &block)
    @defined_matchers ||= []
    @defined_matchers << name
    @current = name
    instance_exec(:attr, &block)
  end

  def match(&block)
    @match_blocks ||= {}
    @match_blocks[@current] = block
  end

  def failure_message(&block)
    @failure_message_blocks ||= {}
    @failure_message_blocks[@current] = block
  end

  def failure_message_when_negated(&block)
    @failure_message_when_negated_blocks ||= {}
    @failure_message_when_negated_blocks[@current] = block
  end

  def description(&block)
    @description_blocks ||= {}
    @description_blocks[@current] = block
  end

  def diffable; end
end
