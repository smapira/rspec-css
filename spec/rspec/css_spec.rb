# frozen_string_literal: true

require 'spec_helper'

describe RSpec::CSS do
  it 'has a version number' do
    expect(RSpec::CSS::VERSION).not_to be nil
  end

  let(:matchers) { MatchersMock.new }
  before { RSpec::CSS.register(matchers) }

  it 'registers :be_valid_on matcher' do
    expect(matchers.defined_matchers).to include(:eq_screen_css)
  end
end
