# frozen_string_literal: true

require 'spec_helper'

RSpec.describe 'Validation', type: :feature do
  let(:files_path) { 'spec/fixtures/files/' }

  before do
    dir_name = File.dirname(files_path)
    FileUtils.mkdir_p(dir_name) unless File.directory?(dir_name)
    Capybara.app = Rack::File.new(files_path)
    Capybara.default_driver = :selenium_chrome_headless
  end

  context 'with valid' do
    it 'may see the valid response', skip: ENV.fetch('CI') { false } do
      visit 'sample.html'
      RSpec::CSS.print_silk("#{files_path}css.yml", page) unless File.exist? "#{files_path}css.yml"
      css_file = File.open "#{files_path}css.yml", 'r'
      expect(page).to eq_screen_css(css_file.read)
    end
  end
end
