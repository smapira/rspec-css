# rspec-css

![Ruby](https://img.shields.io/badge/Ruby-CC342D?style=for-the-badge&logo=ruby&logoColor=white)
[![Gem Version](https://badge.fury.io/rb/rspec-css.svg)](https://badge.fury.io/rb/rspec-css)
![](https://ruby-gem-downloads-badge.herokuapp.com/rspec-css)
[![Ruby Style Guide](https://img.shields.io/badge/code_style-rubocop-brightgreen.svg)](https://github.com/rubocop-hq/rubocop)
[![CircleCI](https://circleci.com/bb/smapira/rspec-css.svg?style=svg)](https://circleci.com/bb/smapira/rspec-css)

![logo](https://blog.routeflags.com/wp-content/uploads/2022/05/rspec-css.png)

Record your test suite's computed CSS during future test runs for deterministic and accurate tests.

## Features

Automatically records and replays your computed CSS each DOMs and 536 distinct property names interactions with minimal setup/configuration code.

## Motivation

Proud to present our newest line of gem, the test suite series. As you know, the CSS has been needing a large variety of case to bloated. This gem include simplest records and replays for comparing computed CSS each DOMs.
If you would kindly accept the deliverable, this gem willing to display differences the previous CSS and changed new one.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'rspec-css'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install rspec-css

## Usage

### Run tests

Request target view using Capybara, then compare previous record the response CSS.

```ruby
Capybara.default_driver = :selenium_chrome_headless
Capybara.app = Rack::File.new('spec/fixtures/files/')
visit 'sample.html'
files_path = 'spec/fixtures/files/sample.yml'
RSpec::CSS.print_silk(files_path, page) unless File.exist? files_path
css_file = File.open files_path, 'r'
expect(page).to eq_screen_css(css_file.read)
```

### Results
```
(compared using ==)

Diff:
@@ -17,7 +17,7 @@
  animation-timing-function: ease
  appearance: ''
  azimuth: ''
-  backface-visibility: hidden
+  backface-visibility: visible
  background: rgba(0, 0, 0, 0) none repeat scroll 0% 0% / auto padding-box border-box
  background-attachment: scroll
  background-blend-mode: normal
```

## Contributing

Bug reports and pull requests are welcome on Bitbucket at https://bitbucket.org/smapira/rspec-css. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the rspec-css project's codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://bitbucket.org/smapira/rspec-css/blob/master/CODE_OF_CONDUCT.md).

## Аcknowledgments

- [vcr](https://github.com/vcr/vcr)
- [ValidationExamplesMatcher](https://github.com/speee/validation_examples_matcher)
- [Carlos Augusto | Unsplash Photo Community](https://unsplash.com/photos/7xfXMt_uRAw)